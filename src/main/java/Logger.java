/**
 * Created by Valeria on 19/04/2018.
 */
public class Logger {

    private static Logger ourInstance = null;
    private String allLog = "";

    public static Logger getInstance() {
        if (ourInstance == null) {
            ourInstance = new Logger();
        }
        return ourInstance;
    }

    public void writeLog(String s) {
        allLog += s + "\n";
    }

    private Logger() {

    }

    public void destruir() {
        ourInstance = null;
    }
}