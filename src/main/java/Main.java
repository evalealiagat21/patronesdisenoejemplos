/**
 * Created by Valeria on 19/04/2018.
 */
public class Main {

    //El fin de todo esto es ilustrar el uso de Patrones de Diseno
    //Este es un commit para saber que ando haciendo

    public static void main(String[] args) {
        Figura F1  = new Figura(4, "Cuadrado");
        F1.getNlados();
        F1.getDescripcion();


        Navegador N1 = new Navegador("CuadrupleCuad","JJStyleVer");
        N1.getNomNav();
        N1.getVersion();


        Persona P1 = new Persona("JJ", 19, "Otabek");
        P1.getNombre();
        P1.getAnoNac();
        P1.getEdad();

    }

}
