/**
 * Created by Valeria on 19/04/2018.
 */
public class Persona {

    private String nombre;
    private int anoNacimiento;
    private String waifuhusbando;

    public Persona (String nombre, int anoNacimiento, String waifuhusbando) {
        this.nombre = nombre;
        this.anoNacimiento = anoNacimiento;
        this.waifuhusbando = waifuhusbando;

    }

    public int getAnoNac() {
        return anoNacimiento;
    }

    public String getWaifuhusbando() {
        return waifuhusbando;
    }

    public String getNombre() {
        return nombre;
    }

    public int getEdad (){
        int a = 2018 - anoNacimiento;
        return a;
    }

}

