/**
 * Created by Valeria on 19/04/2018.
 */
public class Figura {

    private int Nlados;
    private String Descripcion;

    public Figura (int Nlados, String Descripcion) {
        this.Nlados = Nlados;
        this.Descripcion = Descripcion;
    }

    public int getNlados() {
        return Nlados;
    }

    public String getDescripcion() {
        return Descripcion;
    }



}