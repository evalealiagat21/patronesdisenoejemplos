/**
 * Created by Valeria on 19/04/2018.
 */
public class Navegador {

    private String NomNav;
    private String Version;

    public Navegador(String NomNav, String Version) {
        this.NomNav = NomNav;
        this.Version = Version;
    }

    public String getNomNav() {
        return NomNav;
    }

    public String getVersion() {
        return Version;
    }



}